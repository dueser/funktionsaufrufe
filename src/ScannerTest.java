import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class ScannerTest {

    @Test
    void scanneBeispiel1() throws ParseException {
        String actual = new Scanner().scanne("sin(exp(cos(sqr(123))))");
        String expected = "s(e(c(w(z))))";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurSin() throws ParseException {
        String actual = new Scanner().scanne("sin");
        String expected = "s";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurCos() throws ParseException {
        String actual = new Scanner().scanne("cos");
        String expected = "c";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurExp() throws ParseException {
        String actual = new Scanner().scanne("exp");
        String expected = "e";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurSqr() throws ParseException {
        String actual = new Scanner().scanne("sqr");
        String expected = "w";
        assertEquals(expected, actual);
    }

    @Test
    void scanneEmpty() throws ParseException {
        String actual = new Scanner().scanne("");
        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurEinZiffer() throws ParseException {
        String actual = new Scanner().scanne("1");
        String expected = "z";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurZweiZiffern() throws ParseException {
        String actual = new Scanner().scanne("12");
        String expected = "z";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurZweiKlammerZu() throws ParseException {
        String actual = new Scanner().scanne(")");
        String expected = ")";
        assertEquals(expected, actual);
    }

    @Test
    void scanneNurZweiKlammerAuf() throws ParseException {
        String actual = new Scanner().scanne("(");
        String expected = "(";
        assertEquals(expected, actual);
    }

    @Test
    void scanneFehler1() {
        assertThrows(ParseException.class, () -> new Scanner().scanne("ex"));
    }

    @Test
    void scanneFehler2() {
        assertThrows(ParseException.class, () -> new Scanner().scanne("s"));
    }

    @Test
    void scanneFehler3() {
        assertThrows(ParseException.class, () -> new Scanner().scanne("sqrt"));
    }

    @Test
    void scanneFehler4() {
        assertThrows(ParseException.class, () -> new Scanner().scanne("(q"));
    }

}