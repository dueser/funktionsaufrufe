import nrw.Stack;

public class Parser {

    public boolean parse(String input) {
        if (input.isEmpty()) return true;

        Stack<Character> stack = new Stack<>();
        stack.push('#');
        int zustand = 0;
        for (int i = 0; i < input.length(); i++) {
            var zeichen = input.charAt(i);
            switch (zustand) {
                case 0:
                    if (zeichen == 's' || zeichen == 'c' || zeichen == 'e' || zeichen == 'w') {
                        zustand = 1;
                    } else if (zeichen == 'z') {
                        zustand = 2;
                    } else return false;
                    break;
                case 1:
                    if (zeichen == '(') {
                        stack.push('(');
                        zustand = 0;
                    } else return false;
                    break;
                case 2:
                    if (zeichen == ')' && stack.top() == '(') {
                        stack.pop();
                    } else return false;
                    break;
            }
        }
        return (stack.top() == '#' && zustand == 2);
    }
}
