import java.text.ParseException;

public class Funktionsaufrufe {

    public boolean erkenneSprache(String input){
        Scanner sc = new Scanner();
        Parser pa = new Parser();
        try {
            String ergebnis = sc.scanne(input);
            return pa.parse(ergebnis);
        } catch (ParseException e) {
            return false;
        }
    }


}
