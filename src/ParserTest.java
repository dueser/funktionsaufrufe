import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserTest {

    @Test
    void parseGueltigesBeispiel1() throws ParseException {
        var actual = new Parser().parse("s(e(c(w(z))))");
        assertEquals(true, actual);
    }

    @Test
    void parseGueltigesBeispiel2() throws ParseException {
        var actual = new Parser().parse("z");
        assertEquals(true, actual);
    }

    @Test
    void parseUngueltigesBeispiel1() throws ParseException {
        var actual = new Parser().parse("()");
        assertEquals(false, actual);
    }

    @Test
    void parseUngueltigesBeispiel2() throws ParseException {
        var actual = new Parser().parse("se()");
        assertEquals(false, actual);
    }

    @Test
    void parseUngueltigesBeispiel3() throws ParseException {
        var actual = new Parser().parse("s(z))");
        assertEquals(false, actual);
    }

    @Test
    void parseUngueltigesBeispiel4() throws ParseException {
        var actual = new Parser().parse("s((z)");
        assertEquals(false, actual);
    }


}