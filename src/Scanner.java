import java.text.ParseException;

public class Scanner {

    public String scanne(String input) throws ParseException {
        input += "$";
        String output = "";
        int zustand = 0;
        for (int i = 0; i < input.length(); i++) {
            char zeichen = input.charAt(i);
            switch (zustand) {
                case 0:
                    if (zeichen == 's') {
                        zustand = 1;
                    } else if (zeichen == 'c') {
                        zustand = 4;
                    } else if (zeichen == 'e') {
                        zustand = 6;
                    } else if (Character.isDigit(zeichen)) {
                        zustand = 8;
                    } else if (zeichen == '(') {
                        zustand = 0;
                        output += "(";
                    } else if (zeichen == ')') {
                        zustand = 0;
                        output += ")";
                    } else if (zeichen != '$') {
                        throw new ParseException(input, i);
                    }
                    break;
                case 1:
                    if (zeichen == 'i') {
                        zustand = 2;
                    } else if (zeichen == 'q') {
                        zustand = 3;
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 2:
                    if (zeichen == 'n') {
                        zustand = 0;
                        output += "s";
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 3:
                    if (zeichen == 'r') {
                        zustand = 0;
                        output += "w";
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 4:
                    if (zeichen == 'o') {
                        zustand = 5;
                    }
                    break;
                case 5:
                    if (zeichen == 's') {
                        zustand = 0;
                        output += "c";
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 6:
                    if (zeichen == 'x') {
                        zustand = 7;
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 7:
                    if (zeichen == 'p') {
                        zustand = 0;
                        output += "e";
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
                case 8:
                    if (zeichen == '$') {
                        output += "z";
                    }
                    else if (Character.isDigit(zeichen)) {
                        zustand = 8;
                    }
                    else if (zeichen == ')') {
                        zustand = 0;
                        output += "z)";
                    }
                    else if (zeichen == 's') {
                        zustand = 1;
                        output += "z";
                    }
                    else if (zeichen == 'c') {
                        zustand = 4;
                        output += "z";
                    }
                    else if (zeichen == 'e') {
                        zustand = 6;
                        output += "z";
                    }
                    else if (zeichen == '(') {
                        zustand = 0;
                        output += "z(";
                    } else {
                        throw new ParseException(input, i);
                    }
                    break;
            }

        }
        return output;
    }
}
